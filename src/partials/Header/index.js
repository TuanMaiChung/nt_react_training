import React from 'react';
import { withRouter } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';

import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';

const useStyles = makeStyles(theme => ({
    root: {
        flexGrow: 1,
    },
    links: {
        color: 'white',
        textDecoration: 'none',
        marginRight: '30px'
    }
}));

const Header = props => {
    const classes = useStyles();
    const [value, setValue] = React.useState(0);

    React.useEffect(() => {
        setInitialTab(props.location.pathname);
    }, [props])

    const handleChange = (event, newValue) => {
        setValue(newValue);
        goToPage(newValue);
    };

    const setInitialTab = (url) => {
        if (url.includes("/blog")) { setValue(1); }
        else if (url.includes("/about-us")) { setValue(2); }
        else if (url.includes("/contact-us")) { setValue(3); }
        else { setValue(0); }
    }

    const goToPage = (value) => {
        if (value === 0) { // go to Homepage 
            props.history.push("/");
        }
        if (value === 1) { // go to Blog 
            props.history.push("/blog");
        }
        if (value === 2) { // go to AboutUs 
            props.history.push("/about-us");
        }
        if (value === 3) { // go to ContactUs 
            props.history.push("/contact-us");
        }
    }

    return (
        <div className={classes.root}>
            <AppBar position="static">
                <Tabs
                    value={value}
                    onChange={handleChange}
                >
                    <Tab label="Home" />
                    <Tab label="Blog" />
                    <Tab label="About Us" />
                    <Tab label="Contact Us" />
                </Tabs>
            </AppBar>
        </div>
    )
}

export default withRouter(Header);
