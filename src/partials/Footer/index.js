import React from 'react'

const Footer = () => {
    return (
        <footer>
            <script src="https://www.retainable.io/assets/retainable/rss-embed/retainable-rss-embed.js"></script>
        </footer>
    )
}

export default Footer
