import React from 'react'

const Homepage = () => {
    return (
        <div>
            <h1>Hello visitors,</h1>
            <p>
                Welcome for visiting my home page<br/><br/>
                The time right now is: ⏲️ <strong>{Date()}</strong><br/><br/>
                Have a good day! 🌻
            </p>
        </div>
    )
}

export default Homepage;