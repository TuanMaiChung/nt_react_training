import React, {
    useEffect,
    useState
} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { useParams } from 'react-router-dom';
import data from '../../allPosts.json';

const useStyles = makeStyles({
    img: {
      width: '100%',
      heigth: 'auto'
    },
});

const Post = () => {
    const classes = useStyles();
    const { id } = useParams();
    const [ post, setPost ] = useState(null);

    useEffect(() => {
        let result = data.find(i => i.id.toString() === id.toString());
        if(result) setPost(result); 
    }, [id])

    return (
        <div>
            {
                (post != null) ? (
                    <>
                        <img src={post.imageUrl} className={classes.img} />
                        <h2>{post.title}</h2>
                        <p>{post.content}</p>
                    </> 
                ) : 
                "‼️ Sorry we can't find the post 😔"
            }
        </div>
    )
}

export default Post;
