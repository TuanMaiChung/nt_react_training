import React from 'react';
import skype from '../../skype.png';

const ContactUs = () => {
    return (
        <div>
            <h1>Want to contact me? 😎</h1> 
            <p>
                📧 E-mail: Tuan.MaiChung@nashtechglobal.com<br/><br/> 
                📞 Phone: 0925956347 <br/><br/> 
                <img src={skype} alt="Logo" /> Skype name: live:maichungtuan
            </p>

        </div>
    )
}

export default ContactUs;
