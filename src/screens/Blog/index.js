import React, { useEffect } from 'react';
import ListPost from '../../components/ListPost';
import { CircularProgress, makeStyles } from '@material-ui/core';

import { connect } from 'react-redux';
import { renderBlogRequest } from '../../actions/blog';

const useStyles = makeStyles({
    display_center: {
        textAlign: 'center'
    }
})

const Blog = ({ posts, isLoading, renderBlog }) => {
    const classes = useStyles();

    useEffect(() => {
        setTimeout(() => {
            renderBlog();
        }, 2000);
    }, []);

    return (
        <div>
            <h2>Current blogs</h2>
            {
                isLoading ?
                <div className={classes.display_center}><CircularProgress /></div> : 
                <ListPost posts={posts}></ListPost>
            }
        </div>
    )
}

const mapStateToProps = (states) => {
    const { Blog } = states;
    return { 
        posts: Blog.data, 
        isLoading: Blog.isLoading 
    };
}
const mapDispatchToProps = (dispatch) => ({
    renderBlog: () => dispatch(renderBlogRequest())
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Blog);
