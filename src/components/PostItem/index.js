import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import { Link } from '@material-ui/core';
import { withRouter } from 'react-router-dom';

const useStyles = makeStyles({
    card: {
      maxWidth: 345,
    },
});

const PostItem = (props) => {
    const classes = useStyles();
    const { 
        post, 
        history 
    } = props;

    return (
        <Card className={classes.card}>
            <CardActionArea onClick={() => { history.push(`blog/${post.id}`) }}>
                <CardMedia
                    component="img"
                    alt="Post Image"
                    height="140"
                    image={post.imageUrl}
                />
                <CardContent>
                    <Typography gutterBottom variant="h5" component="h2">
                        {post.title}
                    </Typography>
                    <Typography variant="body2" color="textSecondary" component="p">
                        {post.summary}
                        <Link> [More info]</Link>
                    </Typography>
                </CardContent>
            </CardActionArea>
        </Card>
    )
}

export default withRouter(PostItem);