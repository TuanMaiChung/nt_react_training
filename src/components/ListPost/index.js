import React from 'react';
import PostItem from '../PostItem';
import Grid from '@material-ui/core/Grid';
import { makeStyles } from '@material-ui/core/styles';

const useStyle = makeStyles({
    root: {
        flexGrow: 1,
    }
})

const ListPost = (props) => {
    const classes = useStyle();
    const { posts } = props;

    return (
        <Grid container className={classes.root}>
            <Grid item xs={12}>
                <Grid container spacing={4}>
                    {
                        posts.map(post => (
                            <Grid key={post.id} item>
                                <PostItem post={post}></PostItem>
                            </Grid>
                        ))
                    }
                </Grid>
            </Grid>
        </Grid >
    )
}

export default ListPost
