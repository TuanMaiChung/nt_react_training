import data from '../allPosts.json';

export const BLOG_LOAD_SUCCESS = "[BLOG API] load blog success";
export const BLOG_LOAD_ERROR = "[BLOG API] load blog error";

export function renderBlogRequest() {
    // Get data for blog
    if (data)
        return { type: BLOG_LOAD_SUCCESS, payload: data };
    else
        return { type: BLOG_LOAD_ERROR }
}