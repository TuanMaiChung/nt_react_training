import * as types from '../actions/blog';

const initialState = {
    isLoading: true,
    data: []
}

const BlogReducer = (state = initialState, action ) => {
    switch(action.type) {
        case types.BLOG_LOAD_SUCCESS:
            return {
                ...state,
                isLoading: false,
                data: action.payload
            }
        case types.BLOG_LOAD_ERROR: {
            return {
                ...state,
                isLoading: false,
                data: []
            }
        }
        default:
            return state;
    }
}

export default BlogReducer;