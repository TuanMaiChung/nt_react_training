import { combineReducers } from 'redux';
import BlogReducer from './blog';

const rootReducer = combineReducers({
    Blog: BlogReducer
});

export default rootReducer;