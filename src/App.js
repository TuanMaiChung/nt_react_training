import React from 'react';
import { 
  BrowserRouter, 
  Switch, 
  Route 
} from 'react-router-dom';

import Header from "./partials/Header";
import Footer from "./partials/Footer";
import Homepage from './screens/Homepage';
import AboutUs from './screens/AboutUs';
import ContactUs from './screens/ContactUs';
import Blog from './screens/Blog';
import Post from './screens/Post';
import NotFound from './screens/NotFound';
import createStore from "./store";
import { Provider } from "react-redux";
import './App.css';

const { store } = createStore();

function App() {
  return (
    <Provider store={store}>
      <BrowserRouter>
        <Header></Header>
        <div className="App">
            <Switch>
              <Route exact path="/" component={Homepage} />
              <Route exact path="/about-us" component={AboutUs} />
              <Route exact path="/contact-us" component={ContactUs} />
              <Route exact path="/blog" component={Blog} />
              <Route exact path="/blog/:id" component={Post} />
              <Route path="*" component={NotFound} />
            </Switch>  
        </div>
        <Footer></Footer>
      </BrowserRouter>
    </Provider>
  
  );
}

export default App;
